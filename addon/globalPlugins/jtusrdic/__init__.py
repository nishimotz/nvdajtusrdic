# -*- coding: utf-8 -*-
# Copyright (C) 2014 Takuya Nishimoto <nishimotz@gmail.com>
#This file is covered by the GNU General Public License.
#See the file COPYING for more details.

import globalPluginHandler
import gui
import wx
import os
import addonHandler
import globalVars
from logHandler import log
import jtalkDir
import codecs
import sys
impPath = os.path.abspath(os.path.dirname(__file__))
sys.path.append(impPath)
import plumbum
from plumbum import local
del sys.path[-1]

_addonDir = os.path.join(os.path.dirname(__file__), "..", "..").decode("mbcs")
_curAddon = addonHandler.Addon(_addonDir)
_addonSummary = _curAddon.manifest['summary']
addonHandler.initTranslation()

mecabDictIndex = plumbum.local[os.path.join(os.path.dirname(__file__), "mecab-dict-index.exe")]

def editUserDicSrc(self):
	srcs = jtalkDir.user_dic_srcs()
	if srcs:
		for s in srcs:
			os.startfile(s)
	else:
		fileName = os.path.join(jtalkDir.configDir, 'jtusr.txt')
		with codecs.open(fileName, "w", "utf_8", errors="replace") as f:
			f.writelines([u'足手纏い,,,,名詞,形容動詞語幹,*,*,*,*,足手纏い,アシデマトイ,アシデマトイ,4/6,C1,アシデ マトイ\n'])
		os.startfile(fileName)

def compileUserDic(self):
	log.info('system_dic "%s"' % jtalkDir.dic_dir)
	log.info('configDir "%s"' % jtalkDir.configDir)
	srcs = jtalkDir.user_dic_srcs()
	if not srcs:
		gui.messageBox(_("No source found."),_("Done"),wx.OK)
		return
	for s in srcs:
		u = os.path.join(jtalkDir.configDir, os.path.basename(s).replace('.txt', '.dic'))
		log.info('user_dic %s to %s' % (s, u))
		# mecab-dict-index.exe -d ..\source\synthDrivers\jtalk\dic -u jtusr.dic -f utf-8 -t utf-8 jtusr.txt
		ret = mecabDictIndex['-d', jtalkDir.dic_dir, '-u', u, '-f', 'utf-8', '-t', 'utf-8', s]()
		log.info(ret)
	gui.messageBox(_("Compile done."),_("Done"),wx.OK)


class DictionaryEntryDialog(wx.Dialog):

	# Translators: This is the label for the edit dictionary entry dialog.
	def __init__(self, parent, title=_("Edit Dictionary Entry")):
		super(DictionaryEntryDialog,self).__init__(parent,title=title)
		mainSizer=wx.BoxSizer(wx.VERTICAL)
		settingsSizer=wx.BoxSizer(wx.VERTICAL)
		settingsSizer.Add(wx.StaticText(self,-1,label=_("&Pattern")))
		self.patternTextCtrl=wx.TextCtrl(self,wx.NewId())
		settingsSizer.Add(self.patternTextCtrl)
		settingsSizer.Add(wx.StaticText(self,-1,label=_("&Replacement")))
		self.replacementTextCtrl=wx.TextCtrl(self,wx.NewId())
		settingsSizer.Add(self.replacementTextCtrl)
		settingsSizer.Add(wx.StaticText(self,-1,label=_("&Comment")))
		self.commentTextCtrl=wx.TextCtrl(self,wx.NewId())
		settingsSizer.Add(self.commentTextCtrl)
		mainSizer.Add(settingsSizer,border=20,flag=wx.LEFT|wx.RIGHT|wx.TOP)
		buttonSizer=self.CreateButtonSizer(wx.OK|wx.CANCEL)
		mainSizer.Add(buttonSizer,border=20,flag=wx.LEFT|wx.RIGHT|wx.BOTTOM)
		mainSizer.Fit(self)
		self.SetSizer(mainSizer)
		self.patternTextCtrl.SetFocus()


class DictionaryDialog(gui.settingsDialogs.SettingsDialog):

	def __init__(self,parent):
		self.title = u"辞書の編集"
		self.tempSpeechDict = []
		super(DictionaryDialog, self).__init__(parent)

	def makeSettings(self, settingsSizer):
		dictListID=wx.NewId()
		entriesSizer=wx.BoxSizer(wx.VERTICAL)
		entriesLabel=wx.StaticText(self,-1,label=_("&Dictionary entries"))
		entriesSizer.Add(entriesLabel)
		self.dictList=wx.ListCtrl(self,dictListID,style=wx.LC_REPORT|wx.LC_SINGLE_SEL,size=(450,350))
		self.dictList.InsertColumn(0,_("Pattern"),width=150)
		self.dictList.InsertColumn(1,_("Replacement"),width=150)
		self.dictList.InsertColumn(2,_("Comment"),width=150)
		for entry in self.tempSpeechDict:
			self.dictList.Append(
				(
					entry[0],
					entry[1],
					entry[2],
					)
				)
		self.editingIndex=-1
		entriesSizer.Add(self.dictList,proportion=8)
		settingsSizer.Add(entriesSizer)
		entryButtonsSizer=wx.BoxSizer(wx.HORIZONTAL)
		addButtonID=wx.NewId()
		addButton=wx.Button(self,addButtonID,_("&Add"),wx.DefaultPosition)
		entryButtonsSizer.Add(addButton)
		editButtonID=wx.NewId()
		editButton=wx.Button(self,editButtonID,_("&Edit"),wx.DefaultPosition)
		entryButtonsSizer.Add(editButton)
		removeButtonID=wx.NewId()
		removeButton=wx.Button(self,removeButtonID,_("&Remove"),wx.DefaultPosition)
		entryButtonsSizer.Add(removeButton)
		self.Bind(wx.EVT_BUTTON,self.OnAddClick,id=addButtonID)
		self.Bind(wx.EVT_BUTTON,self.OnEditClick,id=editButtonID)
		self.Bind(wx.EVT_BUTTON,self.OnRemoveClick,id=removeButtonID)
		settingsSizer.Add(entryButtonsSizer)

	def postInit(self):
		self.dictList.SetFocus()

	def onCancel(self,evt):
		super(DictionaryDialog, self).onCancel(evt)

	def onOk(self,evt):
		super(DictionaryDialog, self).onOk(evt)

	def OnAddClick(self,evt):
		entryDialog=DictionaryEntryDialog(self,title=_("Add Dictionary Entry"))
		if entryDialog.ShowModal()==wx.ID_OK:
			self.tempSpeechDict.append(
				(
					entryDialog.patternTextCtrl.GetValue(),
					entryDialog.replacementTextCtrl.GetValue(),
					entryDialog.commentTextCtrl.GetValue()
					)
				)
			self.dictList.Append(
				(
					entryDialog.commentTextCtrl.GetValue(),
					entryDialog.patternTextCtrl.GetValue(),
					entryDialog.replacementTextCtrl.GetValue()
					)
				)
			index=self.dictList.GetFirstSelected()
			while index>=0:
				self.dictList.Select(index,on=0)
				index=self.dictList.GetNextSelected(index)
			addedIndex=self.dictList.GetItemCount()-1
			self.dictList.Select(addedIndex)
			self.dictList.Focus(addedIndex)
			self.dictList.SetFocus()
		entryDialog.Destroy()

	def OnEditClick(self,evt):
		if self.dictList.GetSelectedItemCount()!=1:
			return
		editIndex=self.dictList.GetFirstSelected()
		if editIndex<0:
			return
		entryDialog=DictionaryEntryDialog(self)
		entryDialog.patternTextCtrl.SetValue(self.tempSpeechDict[editIndex][0])
		entryDialog.replacementTextCtrl.SetValue(self.tempSpeechDict[editIndex][1])
		entryDialog.commentTextCtrl.SetValue(self.tempSpeechDict[editIndex][2])
		if entryDialog.ShowModal()==wx.ID_OK:
			self.tempSpeechDict[editIndex] = (
				entryDialog.patternTextCtrl.GetValue(),
				entryDialog.replacementTextCtrl.GetValue(),
				entryDialog.commentTextCtrl.GetValue()
				)
			self.dictList.SetStringItem(editIndex,0,entryDialog.patternTextCtrl.GetValue())
			self.dictList.SetStringItem(editIndex,1,entryDialog.replacementTextCtrl.GetValue())
			self.dictList.SetStringItem(editIndex,2,entryDialog.commentTextCtrl.GetValue())
			self.dictList.SetFocus()
		entryDialog.Destroy()

	def OnRemoveClick(self,evt):
		index=self.dictList.GetFirstSelected()
		while index>=0:
			self.dictList.DeleteItem(index)
			del self.tempSpeechDict[index]
			index=self.dictList.GetNextSelected(index)
		self.dictList.SetFocus()


class GlobalPlugin(globalPluginHandler.GlobalPlugin):
	scriptCategory = unicode(_addonSummary)

	def __init__(self, *args, **kwargs):
		super(GlobalPlugin, self).__init__(*args, **kwargs)
		if globalVars.appArgs.secure:
			return
		self.createMenu()

	def createMenu(self):
		items = gui.mainFrame.sysTrayIcon.menu.GetMenuItems()
		self.toolsMenu = items[1].GetSubMenu()
		self.editItem = self.toolsMenu.Append(wx.ID_ANY,
			# Translators:
			_("Edit JTalk User Dic source"),
			# Translators:
			_("Edit JTalk/Japanese Braille user dictionary source"))
		gui.mainFrame.sysTrayIcon.Bind(
			wx.EVT_MENU,
			editUserDicSrc,
			self.editItem)
		self.compileItem = self.toolsMenu.Append(wx.ID_ANY,
			# Translators:
			_("Compile JTalk User Dic"),
			# Translators:
			_("Compile JTalk/Japanese Braille user dictionary"))
		gui.mainFrame.sysTrayIcon.Bind(
			wx.EVT_MENU,
			compileUserDic,
			self.compileItem)
		#self.dictionaryDialogItem = self.toolsMenu.Append(wx.ID_ANY,
		#	# Translators:
		#	_("Open JTalk User Dic Editor"),
		#	# Translators:
		#	_("Open JTalk User Dic Editor"))
		#gui.mainFrame.sysTrayIcon.Bind(
		#	wx.EVT_MENU,
		#	self.onDictionaryDialog,
		#	self.dictionaryDialogItem)

	def terminate(self):
		try:
			self.toolsMenu.RemoveItem(self.editItem)
			self.toolsMenu.RemoveItem(self.compileItem)
			#self.toolsMenu.RemoveItem(self.dictionaryDialogItem)
		except wx.PyDeadObjectError:
			pass

	def onDictionaryDialog(self, evt):
		if gui.isInMessageBox:
			return
		gui.mainFrame.prePopup()
		d = DictionaryDialog(gui.mainFrame)
		d.Show()
		gui.mainFrame.postPopup()
